using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using lab4;

namespace TestLab4
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void RESFunction()
        {
            double x = 1;
            double expected = 0.08081;
            double ResFunction = lab4.Program.Function(x);
            ResFunction = Math.Round(ResFunction, 5);
            Assert.AreEqual(expected, ResFunction);
        }
    }
}

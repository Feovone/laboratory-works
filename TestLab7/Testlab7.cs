﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Lab7;

namespace TestLab7
{
    [TestClass]
    public class Testlab7
    {
        [TestMethod]
        public void RESFunction()
        {
            double x1 = 1;
            double x2 = 2;
            double expected = 1.26005;
            double ResFunction = Lab7.Form1.Function(x1, x2);
            ResFunction = Math.Round(ResFunction, 5);
            Assert.AreEqual(expected, ResFunction);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
	public class Program
	{
		public static double Function(double x)
		{
			double x1 = 2.76 * x;
			double x2 = 0.5 * x;
			x = Math.Cos(Math.Exp(x2 / x1)) / (4 + x2);
			return x;
		}

		public static void Main(string[] args)
		{
			const double StartX = 10.3;
			const double dX = 0.7;


			double[] arr = new double[10];
			double x = StartX;
			for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
			{
				arr[i] = Function(x);
				x += dX;
			}
			Array.Sort(arr);
			Array.Reverse(arr);

			Console.WriteLine("Вiдсортованi за спаданням значення масиву: ");
			for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
			{
				Console.WriteLine("arr[{0:00}] = {1:0.0000}", i, arr[i]);
			}
			double aMin = arr[arr.GetUpperBound(0)];
			double aMax = arr[arr.GetLowerBound(0)];
			double aAvg = 0;
			for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
			{
				aAvg += arr[i];
			}
			aAvg = aAvg / arr.GetLength(0);
			Console.WriteLine("Min: {0:0.000000}", aMin);
			Console.WriteLine("Max: {0:0.000000}", aMax);
			Console.WriteLine("Avg: {0:0.000000}", aAvg);
			double MAvg = aAvg * 0.9;
			short num = 0;
			double PAvg = aAvg * 1.1;
			for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
			{
				if (PAvg >= arr[i] && arr[i] >= MAvg)
				{
					num++;
				};
			}
			Console.WriteLine("Num R: {0:0}", num);
			Console.ReadKey(true);
		}
	}
}

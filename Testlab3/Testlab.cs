﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics;
using lab3;



namespace Testlab
{
    [TestClass]
    public class TestLab3
    {   private Program.Function operation;
        private double a;
        private double b;
        private int n;
        private double eps;

        [TestInitialize]
        public void BeforeEach()
        {
            operation = x => 2 * x + 1;
        }

        [DataTestMethod]
        [DataRow(1, 3, 0.001, 3, 0.03545)]
        [DataRow(-2, 1, 0.001, 10, 0)]
        [DataRow(-1, 2, 0.001, 10, 6)]
        public void Should_Return_Valid_Value_Of_Integral_01(double a, double b, double eps, int n, double expectedResult)
        {
            double actualResult = Program.Integral(a, b, n, operation);
            Debug.WriteLine(string.Format("Expected {0} and actual {1}", expectedResult, actualResult));
            Assert.IsTrue(Math.Abs(expectedResult - actualResult) <= eps);
        } }
        /*double a = 1;
            double b = 3;
            double n = 3;
            double expected = 0.03545;
            double IntgrlRes = lab3.Program.Integral(a, b, n, lab3.Program.Function);
            IntgrlRes = Math.Round(IntgrlRes, 5);
            Assert.AreEqual(expected, IntgrlRes);
        */
    }
    [TestMethod]
        public void RESFunction()
        {
            double x = 2;
            double expected = 0.01566;
            double FunctionRes = lab3.Program.Function(x);
            FunctionRes = Math.Round(FunctionRes, 5);
            Assert.AreEqual(expected, FunctionRes);
        }
    }

    [TestClass]
    public class TestLab4
    {
        [TestMethod]
        public void RESFunction()
        {
            double x = 1;
            double expected = 0.08081;
            double ResFunction = lab4.Program.Function(x);
            ResFunction = Math.Round(ResFunction, 5);
            Assert.AreEqual(expected, ResFunction);
        }
    }

    [TestClass]
    public class TestLab5
    {
        [TestMethod]
        public void RESFunction()
        {
            
        }
    }

    [TestClass]
    public class TestLab7
    {
        [TestMethod]
        public void RESFunction()
        {
            double x1 = 1;
            double x2 = 2;
            double expected = 1.26005;
            double ResFunction = Lab7.Form1.Function(x1, x2);
            ResFunction = Math.Round(ResFunction, 5);
            Assert.AreEqual(expected, ResFunction);
        }
    }

}

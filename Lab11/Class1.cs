﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab11
{
    class CEmblem
    {
        const int DefaultHeight = 20;
        private Graphics graphics;
        private int _height;
        public int X { get; set; }
        public int Y { get; set; }
        public int Height {
            get { return _height; }
            set{
                _height = value >= 200 ? 200 : value;
                _height = value <= 5 ? 5 : value;
            }
        }
        public CEmblem (Graphics graphics, int X, int Y) {
            this.graphics = graphics;
            this.X = X;
            this.Y = Y;
            this.Height = DefaultHeight;
        }
        public CEmblem(Graphics graphics, int X, int Y, int height) {
            this.graphics = graphics;
            this.X = X;
            this.Y = Y;
            this.Height = height;
        }
        private void Draw(Pen pen) {
            Rectangle forEllipse = new Rectangle(X-Height, Y-Height, 2*Height, 2*Height);
            graphics.DrawEllipse(pen, forEllipse);
            Rectangle rectangle = new Rectangle(X - Height, Y + Height, 2 * Height, 2 * Height);
            graphics.DrawRectangle(pen, rectangle);
            Point pt1 = new Point(X -Height, Y+3*Height);
            Point pt2 = new Point((int)X + Height/3, Y +6 *Height);
            Point pt3 = new Point((int)X + Height , Y + 3 * Height);
            graphics.DrawLine(pen, pt1, pt2);
            graphics.DrawLine(pen, pt1, pt3);
            graphics.DrawLine(pen, pt3, pt2);
        }
        public void Show() {
            Draw(Pens.Red);
        }
        public void Hide() {
            Draw(Pens.White);
        }
        public void Expand() {
            Hide();
            Height++;
            Show();
        }
        public void Expand(int dH)
        {
            Hide();
            Height = Height + dH;
            Show();
        }
        public void Collapse() {
            Hide();
            Height--;
            Show();
        }
        public void Collapse(int dH) {
            Hide();
            Height = Height - dH;
            Show();
        }
        public void Move(int dX, int dY) {
            Hide();
            X = X + dX;
            Y = Y + dY;
            Show();
        }
    }
}

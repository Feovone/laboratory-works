﻿namespace Lab11
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.btn_upFar = new System.Windows.Forms.Button();
            this.btn_up = new System.Windows.Forms.Button();
            this.btn_expand = new System.Windows.Forms.Button();
            this.btn_collapse = new System.Windows.Forms.Button();
            this.btn_down = new System.Windows.Forms.Button();
            this.btn_downFar = new System.Windows.Forms.Button();
            this.btn_leftFar = new System.Windows.Forms.Button();
            this.btn_left = new System.Windows.Forms.Button();
            this.btn_right = new System.Windows.Forms.Button();
            this.btn_rightFar = new System.Windows.Forms.Button();
            this.pnMain = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btn_show = new System.Windows.Forms.Button();
            this.btn_hide = new System.Windows.Forms.Button();
            this.btn_createNew = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cbEmblems = new System.Windows.Forms.ComboBox();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // btn_upFar
            // 
            this.btn_upFar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_upFar.BackgroundImage")));
            this.btn_upFar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_upFar.Location = new System.Drawing.Point(55, 253);
            this.btn_upFar.Name = "btn_upFar";
            this.btn_upFar.Size = new System.Drawing.Size(40, 20);
            this.btn_upFar.TabIndex = 0;
            this.btn_upFar.UseVisualStyleBackColor = true;
            this.btn_upFar.Click += new System.EventHandler(this.btn_upFar_Click);
            // 
            // btn_up
            // 
            this.btn_up.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_up.BackgroundImage")));
            this.btn_up.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_up.Location = new System.Drawing.Point(55, 279);
            this.btn_up.Name = "btn_up";
            this.btn_up.Size = new System.Drawing.Size(40, 20);
            this.btn_up.TabIndex = 1;
            this.btn_up.UseVisualStyleBackColor = true;
            this.btn_up.Click += new System.EventHandler(this.btn_up_Click);
            // 
            // btn_expand
            // 
            this.btn_expand.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_expand.BackgroundImage")));
            this.btn_expand.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_expand.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.btn_expand.Location = new System.Drawing.Point(55, 305);
            this.btn_expand.Name = "btn_expand";
            this.btn_expand.Size = new System.Drawing.Size(40, 20);
            this.btn_expand.TabIndex = 2;
            this.btn_expand.UseVisualStyleBackColor = true;
            this.btn_expand.Click += new System.EventHandler(this.btn_expand_Click);
            // 
            // btn_collapse
            // 
            this.btn_collapse.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_collapse.BackgroundImage")));
            this.btn_collapse.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Zoom;
            this.btn_collapse.Location = new System.Drawing.Point(55, 325);
            this.btn_collapse.Name = "btn_collapse";
            this.btn_collapse.Size = new System.Drawing.Size(40, 20);
            this.btn_collapse.TabIndex = 3;
            this.btn_collapse.UseVisualStyleBackColor = true;
            this.btn_collapse.Click += new System.EventHandler(this.btn_collapse_Click);
            // 
            // btn_down
            // 
            this.btn_down.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_down.BackgroundImage")));
            this.btn_down.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_down.Location = new System.Drawing.Point(55, 351);
            this.btn_down.Name = "btn_down";
            this.btn_down.Size = new System.Drawing.Size(40, 20);
            this.btn_down.TabIndex = 4;
            this.btn_down.UseVisualStyleBackColor = true;
            this.btn_down.Click += new System.EventHandler(this.btn_down_Click);
            // 
            // btn_downFar
            // 
            this.btn_downFar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_downFar.BackgroundImage")));
            this.btn_downFar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_downFar.Location = new System.Drawing.Point(55, 377);
            this.btn_downFar.Name = "btn_downFar";
            this.btn_downFar.Size = new System.Drawing.Size(40, 20);
            this.btn_downFar.TabIndex = 5;
            this.btn_downFar.UseVisualStyleBackColor = true;
            this.btn_downFar.Click += new System.EventHandler(this.btn_downFar_Click);
            // 
            // btn_leftFar
            // 
            this.btn_leftFar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_leftFar.BackgroundImage")));
            this.btn_leftFar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_leftFar.Location = new System.Drawing.Point(3, 305);
            this.btn_leftFar.Name = "btn_leftFar";
            this.btn_leftFar.Size = new System.Drawing.Size(20, 40);
            this.btn_leftFar.TabIndex = 6;
            this.btn_leftFar.UseVisualStyleBackColor = true;
            this.btn_leftFar.Click += new System.EventHandler(this.btn_leftFar_Click);
            // 
            // btn_left
            // 
            this.btn_left.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_left.BackgroundImage")));
            this.btn_left.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_left.Location = new System.Drawing.Point(29, 305);
            this.btn_left.Name = "btn_left";
            this.btn_left.Size = new System.Drawing.Size(20, 40);
            this.btn_left.TabIndex = 7;
            this.btn_left.UseVisualStyleBackColor = true;
            this.btn_left.Click += new System.EventHandler(this.btn_left_Click);
            // 
            // btn_right
            // 
            this.btn_right.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_right.BackgroundImage")));
            this.btn_right.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_right.Location = new System.Drawing.Point(101, 305);
            this.btn_right.Name = "btn_right";
            this.btn_right.Size = new System.Drawing.Size(20, 40);
            this.btn_right.TabIndex = 8;
            this.btn_right.UseVisualStyleBackColor = true;
            this.btn_right.Click += new System.EventHandler(this.btn_right_Click);
            // 
            // btn_rightFar
            // 
            this.btn_rightFar.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("btn_rightFar.BackgroundImage")));
            this.btn_rightFar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btn_rightFar.Location = new System.Drawing.Point(127, 305);
            this.btn_rightFar.Name = "btn_rightFar";
            this.btn_rightFar.Size = new System.Drawing.Size(20, 40);
            this.btn_rightFar.TabIndex = 9;
            this.btn_rightFar.UseVisualStyleBackColor = true;
            this.btn_rightFar.Click += new System.EventHandler(this.btn_rightFar_Click);
            // 
            // pnMain
            // 
            this.pnMain.BackColor = System.Drawing.SystemColors.Window;
            this.pnMain.Location = new System.Drawing.Point(0, 0);
            this.pnMain.Name = "pnMain";
            this.pnMain.Size = new System.Drawing.Size(400, 400);
            this.pnMain.TabIndex = 10;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btn_show);
            this.panel1.Controls.Add(this.btn_hide);
            this.panel1.Controls.Add(this.btn_createNew);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.cbEmblems);
            this.panel1.Controls.Add(this.btn_upFar);
            this.panel1.Controls.Add(this.btn_up);
            this.panel1.Controls.Add(this.btn_rightFar);
            this.panel1.Controls.Add(this.btn_expand);
            this.panel1.Controls.Add(this.btn_right);
            this.panel1.Controls.Add(this.btn_collapse);
            this.panel1.Controls.Add(this.btn_downFar);
            this.panel1.Controls.Add(this.btn_leftFar);
            this.panel1.Controls.Add(this.btn_down);
            this.panel1.Controls.Add(this.btn_left);
            this.panel1.Location = new System.Drawing.Point(405, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(152, 400);
            this.panel1.TabIndex = 11;
            // 
            // btn_show
            // 
            this.btn_show.Location = new System.Drawing.Point(3, 122);
            this.btn_show.Name = "btn_show";
            this.btn_show.Size = new System.Drawing.Size(146, 23);
            this.btn_show.TabIndex = 14;
            this.btn_show.Text = "Показати об\'єкт";
            this.btn_show.UseVisualStyleBackColor = true;
            this.btn_show.Click += new System.EventHandler(this.btn_show_Click);
            // 
            // btn_hide
            // 
            this.btn_hide.Location = new System.Drawing.Point(3, 151);
            this.btn_hide.Name = "btn_hide";
            this.btn_hide.Size = new System.Drawing.Size(146, 23);
            this.btn_hide.TabIndex = 13;
            this.btn_hide.Text = "Приховати об\'єкт";
            this.btn_hide.UseVisualStyleBackColor = true;
            this.btn_hide.Click += new System.EventHandler(this.btn_hide_Click);
            // 
            // btn_createNew
            // 
            this.btn_createNew.Location = new System.Drawing.Point(3, 71);
            this.btn_createNew.Name = "btn_createNew";
            this.btn_createNew.Size = new System.Drawing.Size(146, 23);
            this.btn_createNew.TabIndex = 12;
            this.btn_createNew.Text = "Створити новий об\'єкт";
            this.btn_createNew.UseVisualStyleBackColor = true;
            this.btn_createNew.Click += new System.EventHandler(this.btn_createNew_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(89, 13);
            this.label1.TabIndex = 11;
            this.label1.Text = "Перелік об\'єктів";
            // 
            // cbEmblems
            // 
            this.cbEmblems.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbEmblems.FormattingEnabled = true;
            this.cbEmblems.Location = new System.Drawing.Point(3, 25);
            this.cbEmblems.Name = "cbEmblems";
            this.cbEmblems.Size = new System.Drawing.Size(146, 21);
            this.cbEmblems.TabIndex = 10;
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(560, 402);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.pnMain);
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.Text = "Лабораторна робота 11";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_upFar;
        private System.Windows.Forms.Button btn_up;
        private System.Windows.Forms.Button btn_expand;
        private System.Windows.Forms.Button btn_collapse;
        private System.Windows.Forms.Button btn_down;
        private System.Windows.Forms.Button btn_downFar;
        private System.Windows.Forms.Button btn_leftFar;
        private System.Windows.Forms.Button btn_left;
        private System.Windows.Forms.Button btn_right;
        private System.Windows.Forms.Button btn_rightFar;
        private System.Windows.Forms.Panel pnMain;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btn_show;
        private System.Windows.Forms.Button btn_hide;
        private System.Windows.Forms.Button btn_createNew;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cbEmblems;
    }
}


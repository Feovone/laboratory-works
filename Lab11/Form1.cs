﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab11
{
    public partial class fMain : Form
    {
        int EmblemCount = 0;
        int CurrentEmblemIndex;
        CEmblem[] emblems = new CEmblem[100];

        public fMain()
        {
            InitializeComponent();
        }

        private void btn_createNew_Click(object sender, EventArgs e)
        {
            if (EmblemCount >= 99) { MessageBox.Show("Досягнуто межі кількості об'єктів!"); return; }
            Graphics graphics = pnMain.CreateGraphics();
            CurrentEmblemIndex = EmblemCount;
            emblems [CurrentEmblemIndex] =  new CEmblem(graphics, pnMain.Width / 2, pnMain.Height / 2);
            emblems[CurrentEmblemIndex].Show();
            EmblemCount++;
            cbEmblems.Items.Add("Фігура №" + (EmblemCount - 1).ToString());
            cbEmblems.SelectedIndex = EmblemCount - 1;
        }

        private void btn_hide_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            emblems[CurrentEmblemIndex].Hide();
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            emblems[CurrentEmblemIndex].Show();
        }

        private void btn_expand_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            emblems[CurrentEmblemIndex].Expand(5);
        }

        private void btn_collapse_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            emblems[CurrentEmblemIndex].Collapse(5);
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            emblems[CurrentEmblemIndex].Move(0,-5);
        }

        private void btn_down_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            emblems[CurrentEmblemIndex].Move(0, 5);
        }

        private void btn_right_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            emblems[CurrentEmblemIndex].Move(5, 0);
        }

        private void btn_left_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            emblems[CurrentEmblemIndex].Move(-5, 0);
        }

        private void btn_rightFar_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            for (int i = 0; i < 100; i++) {
                emblems[CurrentEmblemIndex].Move(1, 0);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btn_leftFar_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            for (int i = 0; i < 100; i++)
            {
                emblems[CurrentEmblemIndex].Move(-1, 0);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btn_upFar_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            for (int i = 0; i < 100; i++)
            {
                emblems[CurrentEmblemIndex].Move(0, -1);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btn_downFar_Click(object sender, EventArgs e)
        {
            CurrentEmblemIndex = cbEmblems.SelectedIndex;
            if ((CurrentEmblemIndex > EmblemCount) || (CurrentEmblemIndex < 0)) return;
            for (int i = 0; i < 100; i++)
            {
                emblems[CurrentEmblemIndex].Move(0, 1);
                System.Threading.Thread.Sleep(5);
            }
        }

    }
}

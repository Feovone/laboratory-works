﻿using System;

namespace lab2
{
	class Program
	{
		static void Main(string[] args)
		{
			Console.Write("Введите x1 min: ");
			string sx1Min = Console.ReadLine();
			double x1Min = Double.Parse(sx1Min);

			Console.Write("Введите х1 max: ");
			string sx1Max = Console.ReadLine();
			double x1Max = Double.Parse(sx1Max);

			Console.Write("Ведите приріст dx1: ");
			string sdx1 = Console.ReadLine();
			double dx1 = Double.Parse(sdx1);

			Console.Write("Введите x2 min: ");
			string sx2Min = Console.ReadLine();
			double x2Min = Double.Parse(sx2Min);

			Console.Write("Введите х2 max: ");
			string sx2Max = Console.ReadLine();
			double x2Max = Double.Parse(sx2Max);

			Console.Write("Ведите приріст dx2: ");
			string sdx2 = Console.ReadLine();
			double dx2 = Double.Parse(sdx2);

			double y;
			double x1 = x1Min;
			double x2;
			double cos1;
			double cos2;
			double cosStat = 1;
			while (x1 <= x2Max)
			{
				x2 = x2Min;
				while (x2 <= x2Max)
				{
					cos1 = Math.Cos(x1);
					cos2 = Math.Cos(x2);
					if (cos1 < 0)
					{
						cosStat *= cos1;
					}
					if (cos2 < 0)
					{
						cosStat *= cos2;
					}
					double z = Math.Pow(cos1, 3) + x2;
					y = Math.Pow(z, 1.0 / 2) / (Math.Pow(x1, 13)) + 3 / cos2;
					Console.WriteLine("x1 = {0:00.000e+00}\t\tx2 = {1:00.000e+00}\t\ty = {2:00.000e+00}", x1, x2, y);
					x2 += dx2;
				}
				x1 += dx1;

			}
			Console.WriteLine("Від`ємні cos {0}", cosStat);
			Console.ReadKey();


		}
	}
}

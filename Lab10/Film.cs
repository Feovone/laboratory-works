﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab10
{
   public class Film
    {
        public string Name
        {
            get; set;
        }

        public double Length
        {
            get; set;
        }
        public int Rate
        {
            get; set;
        }
        public int Budget
        {
            get; set;
        }
        public string Country
        {
            get; set;
        }
        public DateTime Release
        {
            get; set;
        }
         public Film(string Name, double Length, int Rate, int Budget, string Country, string Release) {
            this.Name = Name;
            this.Length= Length;
            this.Rate = Rate;
            this.Budget = Budget;
            this.Country = Country;
            DateTime Releasew;
            DateTime.TryParse(Release, out Releasew);
            this.Release = Releasew; 
                }
        public Film() { }

       
    }

}

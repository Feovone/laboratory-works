﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab10
{
    public partial class Fname : Form
    {
        public Fname()
        {
            InitializeComponent();
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void toolStripContainer1_LeftToolStripPanel_Click(object sender, EventArgs e)
        {
            gvFilms.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Length";
            column.Name = "Довжина";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Rate";
            column.Name = "Рейтинг";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Budget";
            column.Name = "Бюджет";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Country";
            column.Name = "Країна";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Release";
            column.Name = "Дата виходу";
            gvFilms.Columns.Add(column);
            bindSrcFilm.Add(new Film("НАЗВАНИЕ", 2.5, 5, 5000, "США", "5/01/2008"));
            EventArgs args = new EventArgs();
            OnResize(args);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Film film = new Film();
            FilmAdd FAdd = new FilmAdd(film);
            if (FAdd.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilm.Add(film);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Film film = (Film)bindSrcFilm.List[bindSrcFilm.Position];
            FilmAdd Fadd = new FilmAdd(film);
            if (Fadd.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilm.List[bindSrcFilm.Position] = film;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?",
                "Видалення запису", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindSrcFilm.RemoveCurrent();
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                "Очистити таблицю?\n\nВсі дані будуть втрачені",
                    "Очищення даних", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindSrcFilm.Clear();
            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }

        private void saveFileDialog1_FileOk(object sender, CancelEventArgs e)
        {

        }

        private void btnSaveAsText_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Текстові файли (*.txt)|*.txt|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у текстовому форматі";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            StreamWriter sw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                sw = new StreamWriter(saveFileDialog.FileName, false, Encoding.UTF8);
                try
                {
                    foreach (Film film in bindSrcFilm.List)
                    {
                        sw.Write(film.Name + "\t" + film.Length + "\t" +
                        film.Rate + "\t" + film.Budget + "\t" +
                        film.Country + "\t" + film.Release + "\t\n");
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sw.Close();
                }
            }
        }

        private void btnSaveAsBinary_Click(object sender, EventArgs e)
        {
            saveFileDialog.Filter = "Файли даних (*.films)|*.films|All files (*.*)|*.*";
            saveFileDialog.Title = "Зберегти дані у бінарному форматі";
            saveFileDialog.InitialDirectory = Application.StartupPath;
            BinaryWriter bw;
            if (saveFileDialog.ShowDialog() == DialogResult.OK)
            {
                bw = new
               BinaryWriter(saveFileDialog.OpenFile()); 
                try
                {
                    foreach (Film film in bindSrcFilm.List)
                    {
                        bw.Write(film.Name);
                        bw.Write(film.Length);
                        bw.Write(film.Rate);
                        bw.Write(film.Budget);
                        bw.Write(film.Country);    
                        bw.Write(Convert.ToString(film.Release));

                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    bw.Close();
                }
            }
        }

        private void btnOpenFromText_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Текстові файли (*.txt)|*.txt|All files (*.*) | *.* ";
            openFileDialog.Title = "Прочитати дані у текстовому форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            StreamReader sr;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilm.Clear();
                sr = new StreamReader(openFileDialog.FileName, Encoding.UTF8);
                string s;
                try
                {
                    while ((s = sr.ReadLine()) != null)
                    {
                        string[] split = s.Split('\t');
                        Film film = new Film(split[0], double.Parse(split[1]), 
                        int.Parse(split[2]), int.Parse(split[3]), split[4], split[5]); 
                        bindSrcFilm.Add(film);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    sr.Close();
                }
            }
        }

        private void btnOpenFromBinary_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "Файли даних (*.films)|*.films|All files (*.*) | *.* ";
            openFileDialog.Title = "Прочитати дані у бінарному форматі";
            openFileDialog.InitialDirectory = Application.StartupPath;
            BinaryReader br;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilm.Clear();
                br = new BinaryReader(openFileDialog.OpenFile());
                try
                {
                    Film film;
                    while (br.BaseStream.Position < br.BaseStream.Length)
                    {
                        film = new Film();
                        for (int i = 1; i <= 8; i++)
                        {
                            switch (i)
                            {
                                case 1:
                                    film.Name = br.ReadString();
                                    break;
                                case 2:
                                    film.Length = br.ReadDouble();
                                    break;
                                case 3:
                                    film.Rate = br.ReadInt32();
                                    break;
                                case 4:
                                    film.Budget = br.ReadInt32();
                                    break;
                                case 5:
                                    film.Country = br.ReadString();
                                    break;
                                case 6:
                                    DateTime Releasew;
                                    DateTime.TryParse(br.ReadString(), out Releasew);
                                    film.Release = Releasew;
                                    break;
                                
                            }
                        }
                        bindSrcFilm.Add(film);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("Сталась помилка: \n{0}", ex.Message,
                   MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                finally
                {
                    br.Close();
                }
            }
        }
        
    }
}

﻿using System;
using Word = Microsoft.Office.Interop.Word;
using System.Globalization;
using System.IO;

namespace Wordproject
{
    class Program
    {
        static void Main(string[] args)
        {
        Start:
            Console.Write("Введите:(c субботами  1, без 0  )\n");
            int sub = Convert.ToInt32(Console.ReadLine());
            Console.Write("{0}\n", sub);
            Console.Write("Введите категорию:(A,B,C,CE)\n");
            string Cat = Console.ReadLine();
            Cat = Cat.ToUpper(new CultureInfo("en-US", false));
            string File = @"D:\TEST\Patterns\Build category " + Cat + ".doc";
            //  string File = Path.Combine(Directory.GetCurrentDirectory(), @"Patterns\", "Build category " + Cat + ".doc");

            //Console.Write("{0}",File);
            Console.Write("Введите номер группы.\n");
            string Name = Console.ReadLine();
            Console.Write("Enter DateStart.\n");
            string temp = Console.ReadLine();
            string DateStart = temp;
            Console.Write("Enter DateEnd.\n");
            temp = Console.ReadLine();
            string DateEnd = temp;
            Console.Write("\n{0} - {1}\n", DateStart, DateEnd);
            DateTime start = DateTime.ParseExact(DateStart, "dd.MM.yyyy", CultureInfo.InvariantCulture);
            DateTime end = DateTime.ParseExact(DateEnd, "dd.MM.yyyy", CultureInfo.InvariantCulture); ;
            DateTime startHapp = start;

            var wordApp = new Word.Application();
            wordApp.Visible = false;
            string happy = "01.01, 07.01, 09.03, 20.04, 01.05, 11.05, 08.06, 29.06, 24.08, 14.10, 25.12";
            //string happy = "01.01, ";
           
            try
            {
                int count = 0;
                int day = 1;
                string date;
                var wordDoc = wordApp.Documents.Open(File);
                int CountHappy = 0;
                if (Cat == "B")
                {
                    count = 40;
                }
                if (Cat == "CE")
                {
                    count = 20;
                }
                if (Cat == "A")
                {
                    count = 26;
                }
                if (Cat == "C")
                {
                    count = 50;
                }
                if (Cat == "DB")
                {
                    count = 60;
                    CountHappy = 5;
                }
                if (Cat == "DC")
                {
                    count = 85;
                    CountHappy = 5;
                }
                if (Cat == "DA")
                {
                    count = 41;
                    CountHappy = 1;
                }
                if (Cat == "DD1")
                {
                    count = 39;
                    CountHappy = -1;
                }
                //if (Cat == "DD")
                {
                    count = 39;
                    CountHappy = -1;
                }


                if (sub == 1)
                {
                    while (startHapp != end.AddDays(1))
                    {
                        date = startHapp.ToShortDateString();
                        date = date.Remove(5);
                        bool H = happy.Contains(date);
                      //  Console.Write("{0} ", H);
                        if (H == true)
                        {
                            CountHappy++;
                           
                        }
                        
                        startHapp = startHapp.AddDays(1);
                    }
                    
                    Console.Write("{0}\n", CountHappy);
                }


                while (start != end.AddDays(1))
                {
                    if(CountHappy < 0 && start.DayOfWeek == DayOfWeek.Friday && day!=1)
                    {
                        start = start.AddDays(1);
                        CountHappy++;
                    }
                    if ((CountHappy > 0 || start.DayOfWeek != DayOfWeek.Saturday) && (start.DayOfWeek != DayOfWeek.Sunday))
                    {
                        date = start.ToShortDateString();
                        date = date.Remove(5);
                        bool H = happy.Contains(date);
                        Console.Write("{0} ", H);
                        if (H == false)
                        {
                            Console.Write("{0}\n", date);
                            Rep("{" + day + "}", date, wordDoc);
                            day++;
                            if (CountHappy > 0 && start.DayOfWeek == DayOfWeek.Saturday)
                            {
                                CountHappy--;
                            }
                        }
                        else
                        {
                            Console.Write("HAPPY DAY {0}\n", date);
                        }
                        if(start.DayOfWeek == DayOfWeek.Saturday)
                        {
                            Console.Write(" Saturday ");
                        }
                        Console.Write(" CountHAppy {0} ", CountHappy);

                    }
                    start = start.AddDays(1);
                }

                


                if (count != day - 1)
                {
                    Console.Write("Неправильно выставлены даты. В файле {0} из " + count + " дней набрано\n", day - 1);
                }
                else
                {
                    Console.Write("Количество дней в категории " + Cat + "(" + count + "). В файле набрано {0} дней\n", day - 1);
                }

                Rep("{NAME}", Name, wordDoc);
                wordDoc.SaveAs(@"D:\TEST\Group " + Cat + " №" + Name + ".doc");
                //string SaveFile = Path.Combine(Directory.GetCurrentDirectory(),@"Groups", "Group " + Cat + " №" + Name + ".doc");
                //  wordDoc.SaveAs(SaveFile);
                //Console.Write("{0}", SaveFile);

                wordDoc.Close();
            }
            catch
            {
                Console.Write("Error");
               
            }
            finally
            {
                wordApp.Quit();
            }
            Console.Write("Сделать ещё группу?");
            Console.ReadKey();
            Console.Clear();
            goto Start;
        }

        static void Rep(string RepF, string text, Word.Document wordDoc)
        {
            var range = wordDoc.Content;
            range.Find.ClearFormatting();
            range.Find.Execute(FindText: RepF, ReplaceWith: text);

        }
    }
}

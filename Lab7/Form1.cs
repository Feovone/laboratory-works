﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab7
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        
        public static double Function(double x1,double x2)
        {
            double ResFunct = Math.Sqrt((x2 * x2 + x1 / x2)
                / (Math.Cos(x1 * x1 * x1 * Math.Pow(x2, 5)) + 2 * x1));
            return ResFunct;
        }
       
        private void ButtCalc_Click(object sender, EventArgs e)
        {
            try
            {
                double x1 = Convert.ToDouble(BoxX1.Text);
                double x2 = Convert.ToDouble(BoxX2.Text);
                double ResFunct = Function(x1,x2);
                string Res = ResFunct.ToString("0.##E+00");
                BoxRes.Text = Res;
                if (x1 > x2)
                {
                    BoxComparable.Text = Convert.ToString(x1);
                }else
                {
                    BoxComparable.Text = Convert.ToString(x2);
                }
                    
            }
            catch
            { 
                MessageBox.Show( "Не правильні данні!");
            }

        }

        private void ButtClear_Click(object sender, EventArgs e)
        {
            BoxX1.Text = "";
            BoxX2.Text = "";
            BoxRes.Text = "";
            BoxComparable.Text = "";
        }

        private void ButtExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }


    }
}

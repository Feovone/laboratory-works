﻿namespace Lab7
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButtCalc = new System.Windows.Forms.Button();
            this.ButtClear = new System.Windows.Forms.Button();
            this.ButtExit = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.BoxX1 = new System.Windows.Forms.TextBox();
            this.BoxX2 = new System.Windows.Forms.TextBox();
            this.BoxRes = new System.Windows.Forms.TextBox();
            this.BoxComparable = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // ButtCalc
            // 
            this.ButtCalc.Location = new System.Drawing.Point(45, 207);
            this.ButtCalc.Name = "ButtCalc";
            this.ButtCalc.Size = new System.Drawing.Size(100, 40);
            this.ButtCalc.TabIndex = 0;
            this.ButtCalc.Text = "Обчислити ";
            this.ButtCalc.UseVisualStyleBackColor = true;
            this.ButtCalc.Click += new System.EventHandler(this.ButtCalc_Click);
            // 
            // ButtClear
            // 
            this.ButtClear.Location = new System.Drawing.Point(188, 207);
            this.ButtClear.Name = "ButtClear";
            this.ButtClear.Size = new System.Drawing.Size(100, 40);
            this.ButtClear.TabIndex = 1;
            this.ButtClear.Text = "Очистити";
            this.ButtClear.UseVisualStyleBackColor = true;
            this.ButtClear.Click += new System.EventHandler(this.ButtClear_Click);
            // 
            // ButtExit
            // 
            this.ButtExit.Location = new System.Drawing.Point(328, 207);
            this.ButtExit.Name = "ButtExit";
            this.ButtExit.Size = new System.Drawing.Size(100, 40);
            this.ButtExit.TabIndex = 2;
            this.ButtExit.Text = "Вихід";
            this.ButtExit.UseVisualStyleBackColor = true;
            this.ButtExit.Click += new System.EventHandler(this.ButtExit_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(59, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Змінна Х1";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(59, 80);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(58, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Змінна Х2";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(59, 122);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Результат";
            // 
            // BoxX1
            // 
            this.BoxX1.Location = new System.Drawing.Point(211, 40);
            this.BoxX1.Name = "BoxX1";
            this.BoxX1.Size = new System.Drawing.Size(217, 20);
            this.BoxX1.TabIndex = 6;
            // 
            // BoxX2
            // 
            this.BoxX2.Location = new System.Drawing.Point(211, 80);
            this.BoxX2.Name = "BoxX2";
            this.BoxX2.Size = new System.Drawing.Size(217, 20);
            this.BoxX2.TabIndex = 7;
            // 
            // BoxRes
            // 
            this.BoxRes.Location = new System.Drawing.Point(211, 122);
            this.BoxRes.Name = "BoxRes";
            this.BoxRes.Size = new System.Drawing.Size(217, 20);
            this.BoxRes.TabIndex = 8;
            // 
            // BoxComparable
            // 
            this.BoxComparable.Location = new System.Drawing.Point(211, 161);
            this.BoxComparable.Name = "BoxComparable";
            this.BoxComparable.Size = new System.Drawing.Size(217, 20);
            this.BoxComparable.TabIndex = 9;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(59, 164);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Найбільше число";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(488, 271);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BoxComparable);
            this.Controls.Add(this.BoxRes);
            this.Controls.Add(this.BoxX2);
            this.Controls.Add(this.BoxX1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.ButtExit);
            this.Controls.Add(this.ButtClear);
            this.Controls.Add(this.ButtCalc);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ButtCalc;
        private System.Windows.Forms.Button ButtClear;
        private System.Windows.Forms.Button ButtExit;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox BoxX1;
        private System.Windows.Forms.TextBox BoxX2;
        private System.Windows.Forms.TextBox BoxRes;
        private System.Windows.Forms.TextBox BoxComparable;
        private System.Windows.Forms.Label label4;
    }
}


﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace Іndividual_task
{
    public partial class Currency_Exchanger : Form
    {
        string[] kurs = new string[3];
        public Currency_Exchanger()
        {
            InitializeComponent();
        }
        private void Bank()
        {
            System.Net.WebClient wc = new System.Net.WebClient() { Encoding = Encoding.UTF8 };
            wc.Headers["User-Agent"] = "Mozilla/5.0";
            String response = wc.DownloadString("http://resources.finance.ua/ru/public/currency-cash.json");
            //CurrencyHeader EUR = new CurrencyHeader();
            var obj = JsonConvert.DeserializeObject<CurrenciesGeneral>(response);
            //           string Rate = System.Text.RegularExpressions.Regex.Match(response, @"<th>USD</th><td><span class=""value - decrease""><span>([0-9]+\.[0-9]+)<span>").Groups[1].Value;
            //obj.USD;
            // var usd = obj.organizations.T(org => org.currencies.USD.ask);
            // var usdRateList = obj.organizations.Select(org => org.currencies.USD);
            // kurs[0] ="";
            int u = 0;
            if (obj.organizations[u].currencies.USD.ask != null)
            {
                kurs[0] = obj.organizations[u].currencies.USD.ask;
            }
            if (obj.organizations[u].currencies.EUR.ask != null)
            {
                kurs[1] = obj.organizations[u].currencies.EUR.ask;
            }
            if (obj.organizations[u].currencies.RUB.ask != null)
            {
                kurs[2] = obj.organizations[u].currencies.RUB.ask;
            }
        }
        private string[] convert_Value(double valueMoney, int currency)
        {
            string[] res = new string[4];
            var temp = System.Globalization.CultureInfo.InvariantCulture;
            // double temp_2;
            double tempCurrency;
            if (currency > -1 && currency < 4)
            {
                if (currency == 0)
                {
                    tempCurrency = valueMoney * Convert.ToDouble(kurs[0], temp);
                    res[0] = "Гривна:" + tempCurrency;
                    tempCurrency = (valueMoney * Convert.ToDouble(kurs[0], temp)) / Convert.ToDouble(kurs[1], temp);
                    res[1] = "Євро:" + tempCurrency;
                    tempCurrency = (valueMoney * Convert.ToDouble(kurs[0], temp)) / Convert.ToDouble(kurs[2], temp);
                    res[2] = "Рубль:" + tempCurrency;
                }
                if (currency == 1)
                {
                    tempCurrency = valueMoney * Convert.ToDouble(kurs[1], temp);
                    res[0] = "Гривна:" + tempCurrency;
                    tempCurrency = (valueMoney * Convert.ToDouble(kurs[1], temp)) / Convert.ToDouble(kurs[0], temp);
                    res[1] = "Доллар:" + tempCurrency;
                    tempCurrency = (valueMoney * Convert.ToDouble(kurs[1], temp)) / Convert.ToDouble(kurs[2], temp);
                    res[2] = "Рубль:" + tempCurrency;
                }
                if (currency == 2)
                {
                    tempCurrency = valueMoney * Convert.ToDouble(kurs[2], temp);
                    res[0] = "Гривна:" + tempCurrency;
                    tempCurrency = (valueMoney * Convert.ToDouble(kurs[2], temp)) / Convert.ToDouble(kurs[1], temp);
                    res[1] = "Євро:" + tempCurrency;
                    tempCurrency = (valueMoney * Convert.ToDouble(kurs[2], temp)) / Convert.ToDouble(kurs[0], temp);
                    res[2] = "Доллар:" + tempCurrency;
                }
                if (currency == 3)
                {
                    tempCurrency = valueMoney / Convert.ToDouble(kurs[0], temp);
                    res[0] = "Доллар:" + tempCurrency;
                    tempCurrency = valueMoney / Convert.ToDouble(kurs[1], temp);
                    res[1] = "Євро:" + tempCurrency;
                    tempCurrency = valueMoney / Convert.ToDouble(kurs[2], temp);
                    res[2] = "Рубль:" + tempCurrency;
                }
            }
            else
            {
                MessageBox.Show("Введите валюту");
                res[3] = "Нет стартовой валюты";
            }

            return res;
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            Bank();
            if (boxValue.Text == "") boxValue.Text = "0";
            double valueMoney = Convert.ToDouble(boxValue.Text);
            int currency = boxCurrency.SelectedIndex;
            string[] res = convert_Value(valueMoney, currency);
            if (res[3] != null)
            {
                ResBox.Text += res[3] + "\r\n";
            }
            else
            {
                ResBox.Text += "*******"+"\r\n"+ res[0] + "\r\n";
                ResBox.Text +=   res[1] + "\r\n";
                ResBox.Text +=  res[2] + "\r\n"+ "*******" + "\r\n";
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Bank();
            //string i = kurs[0];
            ResBox.Text += "Доллар:" + kurs[0] + "\r\n";
            ResBox.Text += "Євро:" + kurs[1] + "\r\n";
            ResBox.Text += "Рубль" + kurs[2] + "\r\n";
        }
    }
}

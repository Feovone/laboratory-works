﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Іndividual_task
{

    public class CurrenciesGeneral
    {
        public string sourceId { get; set; }
        public DateTime date { get; set; }
        public Organization[] organizations { get; set; }
        public Orgtypes orgTypes { get; set; }
        public Currencies currencies { get; set; }
        public Regions regions { get; set; }
        public Cities cities { get; set; }
    }

    public class Orgtypes
    {
        public string _1 { get; set; }
        public string _2 { get; set; }
    }

    public class Currencies
    {
        public string AED { get; set; }
        public string AMD { get; set; }
        public string AUD { get; set; }
        public string AZN { get; set; }
        public string BGN { get; set; }
        public string BRL { get; set; }
        public string BYN { get; set; }
        public string CAD { get; set; }
        public string CHF { get; set; }
        public string CLP { get; set; }
        public string CNY { get; set; }
        public string CZK { get; set; }
        public string DKK { get; set; }
        public string EGP { get; set; }
        public string EUR { get; set; }
        public string GBP { get; set; }
        public string GEL { get; set; }
        public string HKD { get; set; }
        public string HRK { get; set; }
        public string HUF { get; set; }
        public string ILS { get; set; }
        public string INR { get; set; }
        public string IQD { get; set; }
        public string JPY { get; set; }
        public string KGS { get; set; }
        public string KRW { get; set; }
        public string KWD { get; set; }
        public string KZT { get; set; }
        public string LBP { get; set; }
        public string MDL { get; set; }
        public string MXN { get; set; }
        public string NOK { get; set; }
        public string NZD { get; set; }
        public string PKR { get; set; }
        public string PLN { get; set; }
        public string RON { get; set; }
        public string RUB { get; set; }
        public string SAR { get; set; }
        public string SEK { get; set; }
        public string SGD { get; set; }
        public string THB { get; set; }
        public string TJS { get; set; }
        public string TRY { get; set; }
        public string TWD { get; set; }
        public string USD { get; set; }
        public string VND { get; set; }
    }

    public class Regions
    {
        public string ua7oiylpmiow8iy1smaci { get; set; }
        public string ua7oiylpmiow8iy1smac7 { get; set; }
        public string ua7oiylpmiow8iy1smack { get; set; }
        public string ua07oiylpmiow8iy1smadi { get; set; }
        public string ua7oiylpmiow8iy1smac0 { get; set; }
        public string ua7oiylpmiow8iy1smacc { get; set; }
        public string ua7oiylpmiow8iy1smacf { get; set; }
        public string ua7oiylpmiow8iy1smacj { get; set; }
        public string ua7oiylpmiow8iy1smacb { get; set; }
        public string ua7oiylpmiow8iy1smaca { get; set; }
    }

    public class Cities
    {
        public string _7oiylpmiow8iy1smadm { get; set; }
        public string _7oiylpmiow8iy1smadn { get; set; }
        public string _7oiylpmiow8iy1smadi { get; set; }
        public string _7oiylpmiow8iy1smadk { get; set; }
        public string _7oiylpmiow8iy1smadx { get; set; }
        public string _7oiylpmiow8iy1smadj { get; set; }
        public string _7oiylpmiow8iy1smaec { get; set; }
    }

    public class Organization
    {
        public string id { get; set; }
        public int oldId { get; set; }
        public int orgType { get; set; }
        public bool branch { get; set; }
        public string title { get; set; }
        public string regionId { get; set; }
        public string cityId { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string link { get; set; }
        public Currencies1 currencies { get; set; }
    }

    public class Currencies1
    {
        public EUR EUR { get; set; }
        public RUB RUB { get; set; }
        public USD USD { get; set; }
        public AED AED { get; set; }
        public AMD AMD { get; set; }
        public AUD AUD { get; set; }
        public AZN AZN { get; set; }
        public BGN BGN { get; set; }
        public BRL BRL { get; set; }
        public BYN BYN { get; set; }
        public CAD CAD { get; set; }
        public CHF CHF { get; set; }
        public CLP CLP { get; set; }
        public CNY CNY { get; set; }
        public CZK CZK { get; set; }
        public DKK DKK { get; set; }
        public EGP EGP { get; set; }
        public GBP GBP { get; set; }
        public GEL GEL { get; set; }
        public HKD HKD { get; set; }
        public HRK HRK { get; set; }
        public HUF HUF { get; set; }
        public ILS ILS { get; set; }
        public INR INR { get; set; }
        public JPY JPY { get; set; }
        public KRW KRW { get; set; }
        public KWD KWD { get; set; }
        public KZT KZT { get; set; }
        public LBP LBP { get; set; }
        public MDL MDL { get; set; }
        public MXN MXN { get; set; }
        public NOK NOK { get; set; }
        public NZD NZD { get; set; }
        public PLN PLN { get; set; }
        public RON RON { get; set; }
        public SAR SAR { get; set; }
        public SEK SEK { get; set; }
        public SGD SGD { get; set; }
        public THB THB { get; set; }
        public TRY TRY { get; set; }
        public TWD TWD { get; set; }
        public VND VND { get; set; }
        public IQD IQD { get; set; }
        public KGS KGS { get; set; }
        public PKR PKR { get; set; }
        public TJS TJS { get; set; }
    }

    public class EUR
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class RUB
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class USD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class AED
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class AMD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class AUD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class AZN
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class BGN
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class BRL
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class BYN
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class CAD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class CHF
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class CLP
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class CNY
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class CZK
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class DKK
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class EGP
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class GBP
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class GEL
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class HKD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class HRK
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class HUF
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class ILS
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class INR
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class JPY
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class KRW
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class KWD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class KZT
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class LBP
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class MDL
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class MXN
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class NOK
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class NZD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class PLN
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class RON
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class SAR
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class SEK
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class SGD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class THB
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class TRY
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class TWD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class VND
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class IQD
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class KGS
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class PKR
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

    public class TJS
    {
        public string ask { get; set; }
        public string bid { get; set; }
    }

}

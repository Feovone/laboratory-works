﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8
{
    public partial class Menu : Form
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void ButAddF_Click(object sender, EventArgs e)
        {
            Film film = new Film();
            FilmAdd FAdd = new FilmAdd(film);
            if (FAdd.ShowDialog() == DialogResult.OK)
            {
                if (film.Budget == -1)
                {
                    BoxStatus.Text += string.Format("\t\t{0}\n Длина фильма: {1}, Рейтинг фильма: {2}\r\n"
                    , film.Name, film.Length, film.Rate);
                }
                else
                {
                    BoxStatus.Text += string.Format("\t\t{0}\n Длина фильма: {1}, Рейтинг фильма: {2}," +
                    "Бюджет фильма: {3:000.0}, Країна виробник: {4}, День релізу: {5}\r\n"
                    , film.Name, film.Length, film.Rate, film.Budget
                    , film.Country, film.Release.ToShortDateString());
                }

            }
        }


        private void ButExit_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}

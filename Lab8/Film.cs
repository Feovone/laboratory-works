﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Lab8
{
   public class Film
    {
        public string Name
        {
            get; set;
        }

        public double Length
        {
            get; set;
        }
        public int Rate
        {
            get; set;
        }
        public int Budget
        {
            get; set;
        }
        public string Country
        {
            get; set;
        }
        public DateTime Release
        {
            get; set;
        }

       
    }

}

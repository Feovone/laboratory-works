﻿namespace Lab8
{
    partial class FilmAdd
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.backgroundWorker1 = new System.ComponentModel.BackgroundWorker();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.FLeng = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.FRate = new System.Windows.Forms.TrackBar();
            this.FName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label8 = new System.Windows.Forms.Label();
            this.Fdate = new System.Windows.Forms.DateTimePicker();
            this.label6 = new System.Windows.Forms.Label();
            this.FContry = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.FBudget = new System.Windows.Forms.TextBox();
            this.ButAdd = new System.Windows.Forms.Button();
            this.ButDis = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRate)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.FLeng);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.FRate);
            this.groupBox1.Controls.Add(this.FName);
            this.groupBox1.Location = new System.Drawing.Point(33, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(294, 150);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Головна інформація";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(233, 68);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(50, 13);
            this.label7.TabIndex = 5;
            this.label7.Text = "Час,мин";
            // 
            // FLeng
            // 
            this.FLeng.Location = new System.Drawing.Point(127, 65);
            this.FLeng.Name = "FLeng";
            this.FLeng.Size = new System.Drawing.Size(100, 20);
            this.FLeng.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(18, 94);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "Рейтинг";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(18, 65);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Довжина  ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(18, 35);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(42, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Назва ";
            // 
            // FRate
            // 
            this.FRate.Location = new System.Drawing.Point(123, 94);
            this.FRate.Maximum = 5;
            this.FRate.Minimum = 1;
            this.FRate.Name = "FRate";
            this.FRate.Size = new System.Drawing.Size(104, 45);
            this.FRate.TabIndex = 2;
            this.FRate.Value = 1;
            // 
            // FName
            // 
            this.FName.Location = new System.Drawing.Point(127, 32);
            this.FName.Name = "FName";
            this.FName.Size = new System.Drawing.Size(100, 20);
            this.FName.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 5;
            this.label4.Text = "Бюджет";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.Fdate);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.FContry);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.FBudget);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Location = new System.Drawing.Point(33, 189);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(294, 137);
            this.groupBox2.TabIndex = 1;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Додаткова інформація";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(233, 32);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(13, 13);
            this.label8.TabIndex = 6;
            this.label8.Text = "$";
            // 
            // Fdate
            // 
            this.Fdate.Location = new System.Drawing.Point(106, 94);
            this.Fdate.Name = "Fdate";
            this.Fdate.Size = new System.Drawing.Size(121, 20);
            this.Fdate.TabIndex = 5;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(13, 101);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Дата релізу";
            // 
            // FContry
            // 
            this.FContry.Location = new System.Drawing.Point(127, 62);
            this.FContry.Name = "FContry";
            this.FContry.Size = new System.Drawing.Size(100, 20);
            this.FContry.TabIndex = 4;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 65);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(41, 13);
            this.label5.TabIndex = 7;
            this.label5.Text = "Країна";
            // 
            // FBudget
            // 
            this.FBudget.Location = new System.Drawing.Point(127, 29);
            this.FBudget.Name = "FBudget";
            this.FBudget.Size = new System.Drawing.Size(100, 20);
            this.FBudget.TabIndex = 3;
            // 
            // ButAdd
            // 
            this.ButAdd.Location = new System.Drawing.Point(349, 51);
            this.ButAdd.Name = "ButAdd";
            this.ButAdd.Size = new System.Drawing.Size(109, 30);
            this.ButAdd.TabIndex = 6;
            this.ButAdd.Text = "Додати ";
            this.ButAdd.UseVisualStyleBackColor = true;
            this.ButAdd.Click += new System.EventHandler(this.button1_Click);
            // 
            // ButDis
            // 
            this.ButDis.Location = new System.Drawing.Point(349, 98);
            this.ButDis.Name = "ButDis";
            this.ButDis.Size = new System.Drawing.Size(109, 30);
            this.ButDis.TabIndex = 3;
            this.ButDis.Text = "Скасувати";
            this.ButDis.UseVisualStyleBackColor = true;
            this.ButDis.Click += new System.EventHandler(this.button2_Click);
            // 
            // FilmAdd
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(505, 351);
            this.Controls.Add(this.ButDis);
            this.Controls.Add(this.ButAdd);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "FilmAdd";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FRate)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.ComponentModel.BackgroundWorker backgroundWorker1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox FLeng;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TrackBar FRate;
        private System.Windows.Forms.TextBox FName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox FBudget;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox FContry;
        private System.Windows.Forms.DateTimePicker Fdate;
        private System.Windows.Forms.Button ButAdd;
        private System.Windows.Forms.Button ButDis;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
    }
}


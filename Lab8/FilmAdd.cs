﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab8
{
    public partial class FilmAdd : Form
    {
        public Film TheFilm;
        public FilmAdd(Film F)
        {
            TheFilm = F;
            InitializeComponent();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
        }

        private void button1_Click(object sender, EventArgs e)
        {
          // int budget;
            // double leng;


            TheFilm.Name = FName.Text.Trim();
            if (FLeng.Text.Contains(','))
            {
                TheFilm.Length = Convert.ToDouble(FLeng.Text.Trim());
            }
            else
            {
                MessageBox.Show("Введите правильно длину");
                goto Error;
            }
            TheFilm.Rate = FRate.Value;
            // MessageBox.Show(FBudget.Text + "  " + Fdate.Text);
            if (FBudget.Text != "" || FContry.Text != "")
            {
                TheFilm.Budget = Convert.ToInt32(FBudget.Text.Trim());
                TheFilm.Country = FContry.Text.Trim();
                TheFilm.Release = DateTime.Parse(Fdate.Text.Trim());
            }
            else
            {
                TheFilm.Budget = -1;
            }
        
            DialogResult = DialogResult.OK;
        Error:;
        }

        /*private bool Mistakes(out double leng, out int budget)
        {

            leng = -1.0;
            budget = -1;

            /*if (double.TryParse(FLeng.Text, out leng))
            {
                MessageBox.Show("Довжина фільма вказана не правильно, або було введено не коректні символи.");
                return true;
            }

            if (int.TryParse(FBudget.Text, out budget))
            {
                MessageBox.Show("Бюджет фільма вказаний не правильно, або було введено не коректні символи.");
                return true;
            }

            return false;
        }*/

        private void FLeng_TextChanged(object sender, EventArgs e)
        {


        }

        private void FLeng_Leave(object sender, EventArgs e)
        {
            double leng = -1.0;
            if (!double.TryParse(FLeng.Text, out leng))
            {
                MessageBox.Show("Довжина фільма вказана не правильно, або було введено не коректні символи.");
                FLeng.Enabled = false;
            }
            else
            {
                FLeng.Enabled = true;
            }
        }
    }
}

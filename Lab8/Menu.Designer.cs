﻿namespace Lab8
{
    partial class Menu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ButAddF = new System.Windows.Forms.Button();
            this.BoxStatus = new System.Windows.Forms.RichTextBox();
            this.ButExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // ButAddF
            // 
            this.ButAddF.AllowDrop = true;
            this.ButAddF.Location = new System.Drawing.Point(450, 50);
            this.ButAddF.Name = "ButAddF";
            this.ButAddF.Size = new System.Drawing.Size(150, 50);
            this.ButAddF.TabIndex = 0;
            this.ButAddF.Text = "Додати фільм ";
            this.ButAddF.UseVisualStyleBackColor = true;
            this.ButAddF.Click += new System.EventHandler(this.ButAddF_Click);
            // 
            // BoxStatus
            // 
            this.BoxStatus.BackColor = System.Drawing.SystemColors.Window;
            this.BoxStatus.Cursor = System.Windows.Forms.Cursors.Default;
            this.BoxStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F);
            this.BoxStatus.Location = new System.Drawing.Point(25, 25);
            this.BoxStatus.Name = "BoxStatus";
            this.BoxStatus.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.BoxStatus.Size = new System.Drawing.Size(400, 250);
            this.BoxStatus.TabIndex = 1;
            this.BoxStatus.Text = "";
            // 
            // ButExit
            // 
            this.ButExit.Location = new System.Drawing.Point(450, 150);
            this.ButExit.Name = "ButExit";
            this.ButExit.Size = new System.Drawing.Size(150, 50);
            this.ButExit.TabIndex = 2;
            this.ButExit.Text = "Exit";
            this.ButExit.UseVisualStyleBackColor = true;
            this.ButExit.Click += new System.EventHandler(this.ButExit_Click_1);
            // 
            // Menu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(634, 311);
            this.Controls.Add(this.ButExit);
            this.Controls.Add(this.BoxStatus);
            this.Controls.Add(this.ButAddF);
            this.Name = "Menu";
            this.Text = "Form2";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button ButAddF;
        private System.Windows.Forms.RichTextBox BoxStatus;
        private System.Windows.Forms.Button ButExit;
    }
}
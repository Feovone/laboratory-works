﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab14
{
    class Program
    {
        static List<Film> films;
        static void Main(string[] args)
        {
            films = new List<Film>();
            FileStream fs = new FileStream("1.films", FileMode.Open);
            BinaryReader reader = new BinaryReader(fs);
            try
            {
                Film film ;
                Console.WriteLine("Читаємо данi з файлу...\n");
                while (reader.BaseStream.Position < reader.BaseStream.Length)
                {
                    film = new Film();
                    for (int i = 1; i <= 8; i++)
                    {
                        switch (i)
                        {
                            case 1:
                                film.Name = reader.ReadString();
                                break;
                            case 2:
                                film.Length = reader.ReadDouble();
                                break;
                            case 3:
                                film.Rate = reader.ReadInt32();
                                break;
                            case 4:
                                film.Budget = reader.ReadInt32();
                                break;
                            case 5:
                                film.Country = reader.ReadString();
                                break;
                            case 6:
                                DateTime Releasew;
                                DateTime.TryParse(reader.ReadString(), out Releasew);
                                film.Release = Releasew;
                                break;
                        }
                    }
                    films.Add(film);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Сталась помилка: {0}", ex.Message);
            }
            finally
            {
                reader.Close();
            }
            Console.WriteLine("Несортований перелiк мiст: {0}",
            films.Count); PrintTowns(); films.Sort();
            Console.WriteLine("Сортований перелiк мiст: {0}", films.Count);
            PrintTowns();
            Console.WriteLine("Додаємо новий запис: Одеса");
            Film filmOdesa = new Film("Н=АЗВАНИЕ", 2.5, 5, 59000, "Канада", "5/01/2009");
            films.Add(filmOdesa);
            films.Sort();
            Console.WriteLine("Перелiк мiст: {0}", films.Count);
            PrintTowns();
            Console.WriteLine("Видаляємо останнє значення");
            films.RemoveAt(films.Count - 1);
            Console.WriteLine("Перелiк мiст: {0}", films.Count);
            PrintTowns();
            Console.ReadKey();
        }
        static void PrintTowns()
        {
            foreach (Film film in films)
            {
                Console.WriteLine(film.Info().Replace('і', 'i'));
            }
            Console.WriteLine();
        }
    }
}

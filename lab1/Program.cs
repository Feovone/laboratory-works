﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab1._0
{
	class Program
	{
		static void Main(string[] args)
		{

			Console.Write("Введите початкове xmin: ");
			string sxMin = Console.ReadLine();
			double xMin = double.Parse(sxMin);

			Console.Write("Введите кінцеве Xmax: ");
			string sxMax = Console.ReadLine();
			double xMax = double.Parse(sxMax);

			Console.Write("Введите приріст dx: ");
			sxMax = Console.ReadLine();
			double dx = double.Parse(sxMax);

			double x = xMin;
			double y;
			double x2 = xMax;
			double cos1;
			double cosS = 1;
			while (x <= xMax)
			{
				cos1 = Math.Cos(Math.Pow(x, 4));
				cosS *= cos1;
				double z = 0.6 * x * Math.Sin(x2) * cos1;
				if (z <= 0)
				{
					Console.Write("Z<=0\n");
				}
				else
				{

					y = Math.Log(x2) / Math.Pow(z, 1.0 / 5);
					Console.WriteLine("x = {0}\t\t y = {1}\t\t z={2}", x, y, z);

				}
				x += dx;
			}
			Console.WriteLine("cos = {0}", cosS);
			Console.ReadKey();
		}
	}
}





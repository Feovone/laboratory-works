﻿using System;
using System.Collections.Generic;

namespace lab5
{

    class Program
    {
        static void Menu()
        {
            List<Film> Films = new List<Film>(1);
            int i = -1;
            bool status = false;
            string sanswer = "";
            int answer;
            while (sanswer != "Q")
            {
                Console.Write("New base of best film!\n");
                Console.Write("What you want to do?\n");
                Console.Write("[1]Add new Film\n");
                Console.Write("[2]Status base\n");
                Console.Write("[3]Delete Film\n");
                sanswer = Console.ReadLine();
                while (sanswer == "")
                {
                    Console.Write("Enter correct answer.\n");
                    sanswer = Console.ReadLine();
                }
                answer = int.Parse(sanswer);
                if (answer == 1)
                {
                    status = true;
                    i++;
                    Films.Add(new Film() { });
                    // Console.WriteLine("{0}", i);
                    MovieT(Films, i);
                }
                if (answer == 2)
                {
                    if (status == true)
                    {
                        Status(Films, i);
                    }
                    else
                    {
                        Console.Write("Add though one book\n\n");
                    }
                }
                if (answer == 3)
                {
                    if (status == true)
                    {
                        Console.Write("What object you want to delete?");
                        Delete(Films, i);
                        i--;
                    }
                    else
                    {
                        Console.Write("Base does not have anything film\n");
                    }
                }
            }
        }
        static void MovieT(List<Film> Films, int i)
        {
            int res;
            string lengthpro;
            string[] temp;
            bool isNumric;
            Console.Write("Enter film title: = ");
            Films[i].Name = Console.ReadLine();
            while (true)
            {
                Console.Write("Enter movie length (exampl: 1:20) = ");
                lengthpro = Console.ReadLine();
                temp = lengthpro.Split(':');

                if (lengthpro.Contains(":") && temp[0] != "" && temp[1] != "")
                {
                    Films[i].LengthToTime(temp);
                    break;
                }
                else
                {
                    Console.Write("Enter correct answer.\n");
                }

            }

            Console.Write("Enter movie  rate = ");
            do
            {
                lengthpro = Console.ReadLine();
                isNumric = int.TryParse(lengthpro, out res);
                if (isNumric == false || res < 0 || res > 100)
                {
                    Console.Write("Enter correct rate, in the range from 0 and to 100\n");
                }
            } while (isNumric != true || res < 0 || res > 100);
            Films[i].Rate = Convert.ToInt32(res);

            Console.Write("Enter Budget and Country(exampl: 100000,Canada) = ");
            while (true)
            {
                lengthpro = Console.ReadLine();
                temp = lengthpro.Split(',');
                isNumric = int.TryParse(temp[0], out res);
                if (lengthpro.Contains(",") && temp[0] != "" && temp[1] != "" && isNumric == true)
                {
                    Films[i].BudgetAndCountry(temp);
                    break;
                }
                else
                {
                    Console.Write("Enter correct answer.\n");
                }
            }

            Console.Write("Enter date of premier(exampl: 26,04,1986) = ");
            while (true)
            {
                lengthpro = Console.ReadLine();
                temp = lengthpro.Split(',');
                if (lengthpro.Contains(",") && temp[0] != "" && temp[1] != "" && temp[2] != ""
                        && Convert.ToInt32(temp[0]) < 31 && Convert.ToInt32(temp[1]) < 12)
                {
                    Films[i].DateToData(temp);
                    Console.Write("\n");
                    break;
                }
                else
                {
                    Console.Write("Enter correct answer.\n");
                }
            }
        }
        static void Status(List<Film> Films, int i)
        {
            int j = i;
            while (j != -1)
            {
                Console.Write("****************************");
                Console.Write("Film: {0}", Films[j].Name);
                Console.Write("****************************\n");
                Films[j].TimeToLength();
                Console.WriteLine("Film rate: {0}", Films[j].Rate);
                Films[j].RewBudget();
                Console.WriteLine("\nContry product: {0}", Films[j].Country);
                Console.WriteLine("Date of realise: {0:00},{1:00},{2:00}", Films[j].Day, Films[j].Month, Films[j].Year);
                Console.Write("****************************\n");
                j--;
            }
            Console.Write("Back to menu [M]");
            ConsoleKeyInfo pressedKey = Console.ReadKey();
            while (true)
            {
                if (pressedKey.Key != ConsoleKey.M)
                {
                    pressedKey = Console.ReadKey();
                }
                else
                {
                    break;
                }

            }

            Console.WriteLine();

        }
        static void Delete(List<Film> Films, int i)
        {
            int j = 0;
            while (j != i + 1)
            {
                Console.WriteLine("{0}){1}", j + 1, Films[j].Name);
                j++;
            }
            j = Convert.ToInt32(Console.ReadLine());
            Films.Remove(Films[j - 1]);
        }


        static void Main(string[] args)
        {
            Menu();
            Console.ReadKey();
        }

    }
}


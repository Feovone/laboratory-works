﻿using System;
using System.Collections.Generic;
using System.Text;

namespace lab5
{
    class Film
    {
        public string Name
        {
            get; set;
        }

        public int Time
        {
            get; set;
        }
        public int Rate
        {
            get; set;
        }
        public string Budget
        {
            get; set;
        }
        public string Country
        {
            get; set;
        }
        public int Day
        {
            get; set;
        }
        public int Month
        {
            get; set;
        }
        public int Year
        {
            get; set;
        }
        public void LengthToTime(string[] time)
        {
            int h = Convert.ToInt32(time[0]);
            int min = Convert.ToInt32(time[1]);
            this.Time = h * 60 + min;
        }
        public void TimeToLength()
        {
            string[] time = new string[2];
            int h = 0;
            int a = this.Time;
            while (a > 60)
            {
                a -= 60;
                h++;
            }
            time[0] = Convert.ToString(h);
            time[1] = Convert.ToString(a);
            Console.WriteLine("Lenght film: " + time[0] + ":" + time[1]);
        }
        public void BudgetAndCountry(string[] BandC)
        {
            this.Budget = BandC[0];
            this.Country = BandC[1];
        }
        public void RewBudget()
        {
            string budget;
            budget = this.Budget;
            int i = 0;
            short Del = 0;
            int ros = budget.Length;
            Console.Write("Budget film :");
            while (ros >= 3)
            {
                ros -= 3;
            }
            if (ros == 1)
            {
                Console.Write(budget[i]);
                i++;
                Console.Write(",");
            }
            if (ros == 2)
            {
                Console.Write(budget[i]);
                i++;
                Console.Write(budget[i]);
                i++;
                Console.Write(",");
            }
            while (i < budget.Length)
            {
                Console.Write(budget[i]);
                i++;
                Del++;
                if (Del % 3 == 0 && i + 1 < budget.Length)
                {
                    Console.Write(",");
                }
            }
        }
        public void DateToData(string[] temp)
        {
            this.Day = Convert.ToInt32(temp[0]);
            this.Month = Convert.ToInt32(temp[1]);
            this.Year = Convert.ToInt32(temp[2]);
        }
        public void RewDate()
        {

        }
    }

}

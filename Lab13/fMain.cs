﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab13
{
    public partial class Fname : Form
    {
        public Fname()
        {
            InitializeComponent();
        }

        private void toolTip1_Popup(object sender, PopupEventArgs e)
        {

        }

        private void toolStripContainer1_LeftToolStripPanel_Click(object sender, EventArgs e)
        {
            gvFilms.AutoGenerateColumns = false;
            DataGridViewColumn column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Name";
            column.Name = "Назва";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Length";
            column.Name = "Довжина";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Rate";
            column.Name = "Рейтинг";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Budget";
            column.Name = "Бюджет";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Country";
            column.Name = "Країна";
            gvFilms.Columns.Add(column);
            column = new DataGridViewTextBoxColumn();
            column.DataPropertyName = "Release";
            column.Name = "Дата виходу";
            gvFilms.Columns.Add(column);
            bindSrcFilm.Add(new Film("НАЗВАНИЕ", 2.5, 5, 5000, "США", "5/01/2008"));
            EventArgs args = new EventArgs();
            OnResize(args);
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            Film film = new Film();
            FilmAdd FAdd = new FilmAdd(film);
            if (FAdd.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilm.Add(film);
            }
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            Film film = (Film)bindSrcFilm.List[bindSrcFilm.Position];
            FilmAdd Fadd = new FilmAdd(film);
            if (Fadd.ShowDialog() == DialogResult.OK)
            {
                bindSrcFilm.List[bindSrcFilm.Position] = film;
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Видалити поточний запис?",
                "Видалення запису", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Warning) == DialogResult.OK)
            {
                bindSrcFilm.RemoveCurrent();
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(
                "Очистити таблицю?\n\nВсі дані будуть втрачені",
                    "Очищення даних", MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) == DialogResult.OK)
            {
                bindSrcFilm.Clear();
            }
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Закрити застосунок?", "Вихід з програми",
                MessageBoxButtons.OKCancel,
                MessageBoxIcon.Question) == DialogResult.OK)
            {
                Application.Exit();
            }
        }
    }
}

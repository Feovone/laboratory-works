﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Lab12
{
    class CEmblem : CFigure
    {   
        private int _height;
        public int Height
        {
            get { return _height; }
            set
            {
                _height = value >= 200 ? 200 : value;
                _height = value <= 5 ? 5 : value;
            }
        }
        private CTriangle triangle;
        private CCircle circle;
        private CRectangle rectangle;
        public CEmblem (Graphics graphics, int X, int Y, int height)
        {
            triangle = new CTriangle(graphics,X-2*height, Y, height);
            circle = new CCircle(graphics, X, Y, height);
            rectangle = new CRectangle(graphics, X + 2 * height, Y, height,height);

        }

        protected override void Draw(Pen pen)
        {
            triangle.Show();
            circle.Show();
            rectangle.Show();
        }
        
        public override void Expand(int dX)
        {
            Hide();
            triangle.Expand(dX);
            triangle.Move(-2 * dX, 0);
            circle.Expand(dX);
            rectangle.Expand(dX);
            rectangle.Move(2 * dX, 0);
            Show();
        }

        public override void Collapse(int dX)
        {
            Hide();
            triangle.Collapse(dX);
            triangle.Move(2 * dX, 0);
            circle.Collapse(dX);
            rectangle.Collapse(dX);
            rectangle.Move(-2 * dX, 0);
            Show();
        }

        public override void Move(int dX, int dY)
        {
            Hide();
            triangle.Move(dX, dY);
            circle.Move(dX, dY);
            rectangle.Move(dX, dY);
            Show();
        }
        public override void Hide()
        {
            triangle.Hide();
            rectangle.Hide();
            circle.Hide();
        }
        public override void Show()
        {
            triangle.Show();
            rectangle.Show();
            circle.Show();
        }
    }
}

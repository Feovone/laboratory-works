﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab12
{
    public partial class fMain : Form
    {
        int FiguresCount = 0;
        int CurrentFigureIndex;
        CFigure[] figures = new CFigure[100];

        public fMain()
        {
            InitializeComponent();
            cbFigureType.Items.Add("Коло");
            cbFigureType.Items.Add("Прямокутник");
            cbFigureType.Items.Add("Трикутник");
            cbFigureType.Items.Add("Емблема");
        }

        private void btn_createNew_Click(object sender, EventArgs e)
        {
            if (FiguresCount >= 99) { MessageBox.Show("Досягнуто межі кількості об'єктів!"); return; }
            Graphics graphics = pnMain.CreateGraphics();
            CurrentFigureIndex = FiguresCount;
            if (cbFigureType.SelectedIndex == 0)      
            {
                figures[CurrentFigureIndex] =  new CCircle(graphics, pnMain.Width / 2, pnMain.Height / 2, 20);
                cbFigures.Items.Add("Фігура №" + (FiguresCount).ToString() +  " [коло]");
            }
            else if (cbFigureType.SelectedIndex == 1)
            {
                figures[CurrentFigureIndex] = new CRectangle(graphics, pnMain.Width / 2, pnMain.Height / 2, 20, 20);
                cbFigures.Items.Add("Фігура №" + (FiguresCount).ToString() +  " [прямокутник]");
            }
            else if (cbFigureType.SelectedIndex == 2)
            {
                figures[CurrentFigureIndex] = new CTriangle(graphics, pnMain.Width / 2, pnMain.Height / 2, 20);
                cbFigures.Items.Add("Фігура №" + (FiguresCount).ToString() +  " [трикутник]");
            }
            else if (cbFigureType.SelectedIndex == 3)
            {
                figures[CurrentFigureIndex] = new CEmblem (graphics, pnMain.Width/2, pnMain.Height/2, 20);
                cbFigures.Items.Add("Фігура №" + (FiguresCount).ToString() + " [варіант 18]");
            }
            figures[CurrentFigureIndex].Show();
            FiguresCount++;
            cbFigures.SelectedIndex = FiguresCount -1;
        }

        private void btn_hide_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Hide();
        }

        private void btn_show_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Show();
        }

        private void btn_expand_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Expand(5);
        }

        private void btn_collapse_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Collapse(5);
        }

        private void btn_up_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Move(0,-5);
        }

        private void btn_down_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Move(0, 5);
        }

        private void btn_right_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Move(5, 0);
        }

        private void btn_left_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            figures[CurrentFigureIndex].Move(-5, 0);
        }

        private void btn_rightFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            for (int i = 0; i < 100; i++) {
                figures[CurrentFigureIndex].Move(1, 0);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btn_leftFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            for (int i = 0; i < 100; i++)
            {
                figures[CurrentFigureIndex].Move(-1, 0);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btn_upFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            for (int i = 0; i < 100; i++)
            {
                figures[CurrentFigureIndex].Move(0, -1);
                System.Threading.Thread.Sleep(5);
            }
        }

        private void btn_downFar_Click(object sender, EventArgs e)
        {
            CurrentFigureIndex = cbFigures.SelectedIndex;
            if ((CurrentFigureIndex > FiguresCount) || (CurrentFigureIndex < 0)) return;
            for (int i = 0; i < 100; i++)
            {
                figures[CurrentFigureIndex].Move(0, 1);
                System.Threading.Thread.Sleep(5);
            }
        }

    }
}
